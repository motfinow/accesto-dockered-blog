Running application
===================

`docker-compose -f docker-compose.yml -f docker-compose.dev.yml up`

Api doc will be available at http://localhost:8080/app_dev.php/doc

There is a login button next to each endpoint (in sandbox),
you can login using following data:

- admin:admin
- user:user


Tasks
=====

1. Edit and create blog post
Add new endpoints, one for creating blog post and one for editing.
Title, content and tags should be editable.
Tags will be send to api endpoint in form of simple string, each tag separated by semicolon.
Security:
Available only for admin

2. Allow to publish blog post do different services (targets)
Provide solution for publish post to e.g. facebook, twitter.
Do not implement integration with those services, just mock this behaviour.
Url will look like this provided in `BlogPostController` /blog-post/{post}/{target}
If such target does not exists (e.g. someone will send `google` as target) throw an TargetNotExistException
and implement listener for it to return some user friendly response in such case.
Hint:
Use tagged services.
Security:
Action available only for admin.

3. Secure BlogPostController::listPostsAction, make it available for admin and user.




How to test
===========

## Newman

1. Install newman
https://learning.getpostman.com/docs/postman/collection_runs/command_line_integration_with_newman/

2. Check your environment url in accesto_localhost.postman_environment.json
In case it has changed, you can create your own env file by copy and change url in a file.

3. Go to tests DIR and run:
```bash
newman run accesto_blog_post.postman_collection.json -e accesto_localhost.postman_environment.json
```

## Postman

Optionaly you can import collection to your postman app and test it there. 
Do not forget to load your environment also.

NOTE
Normally such tests run by API postman teams with API_KEY. 
I did it in this way just to show how I test API by postman