<?php

namespace AppBundle\Event;

/**
 * Class BlogPostUpdatedEvent
 * @package AppBundle\Event\BlogPost
 */
class BlogPostUpdatedEvent extends AbstractBlogPostAwareEvent
{
}
