<?php

namespace AppBundle\Event;

use AppBundle\Entity\BlogPost;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class AbstractBlogPostAwareEvent
 * @package AppBundle\Event\BlogPost
 */
abstract class AbstractBlogPostAwareEvent extends Event
{
    protected $blogPost;

    public function __construct(BlogPost $blogPost)
    {
        $this->blogPost = $blogPost;
    }

    /**
     * @return BlogPost
     */
    public function getBlogPost()
    {
        return $this->blogPost;
    }
}
