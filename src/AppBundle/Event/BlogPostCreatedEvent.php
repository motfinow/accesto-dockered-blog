<?php

namespace AppBundle\Event;

/**
 * Class BlogPostCreatedEvent
 * @package AppBundle\Event\BlogPost
 */
class BlogPostCreatedEvent extends AbstractBlogPostAwareEvent
{
}
