<?php

namespace AppBundle\DependencyInjection\Compiler;

use AppBundle\Service\BlogPost\TargetContainer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class BlogPostTargetPass
 * @package AppBundle\DependencyInjection\Compiler
 */
class BlogPostTargetPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(TargetContainer::class)) {
            return;
        }

        $definition = $container->findDefinition(TargetContainer::class);

        $taggedServices = $container->findTaggedServiceIds('blog_post.target');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addTarget', array(new Reference($id)));
        }
    }
}
