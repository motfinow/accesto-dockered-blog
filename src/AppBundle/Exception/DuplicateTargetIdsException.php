<?php

namespace AppBundle\Exception;

/**
 * Class DuplicateTargetIdsException
 * @package AppBundle\Exception
 */
class DuplicateTargetIdsException extends \LogicException
{
}
