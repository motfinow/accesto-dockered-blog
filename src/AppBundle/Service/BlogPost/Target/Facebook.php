<?php

namespace AppBundle\Service\BlogPost\Target;

use AppBundle\Contract\BlogPost\ReadBlogPostInterface;
use AppBundle\Contract\BlogPost\TargetInterface;

class Facebook implements TargetInterface
{
    /**
     * @return string
     */
    public function getId(): string
    {
        return 'facebook';
    }

    /**
     * @param ReadBlogPostInterface $blogPost
     */
    public function publish(ReadBlogPostInterface $blogPost): void
    {
        // TODO: Implement publish() method.
    }
}
