<?php

namespace AppBundle\Service\BlogPost;

use AppBundle\Contract\BlogPost\TargetInterface;
use AppBundle\Exception\DuplicateTargetIdsException;
use AppBundle\Exception\TargetNotExistsException;
use Psr\Container\ContainerInterface;

class TargetContainer implements ContainerInterface
{
    protected $targets = [];

    public function addTarget(TargetInterface $target): void
    {
        if (isset($this->targets[$target->getId()])) {
            throw new DuplicateTargetIdsException($target->getId().' is already defined');
        }

        $this->targets[$target->getId()] = $target;
    }

    /**
     * @param string $id
     * @return TargetInterface
     * @throws TargetNotExistsException
     */
    public function get($id)
    {
        if (!$this->has($id)) {
            throw new TargetNotExistsException($id.' target is not defined');
        }

        return $this->targets[$id];
    }

    /**
     * @param string $id
     * @return bool
     */
    public function has($id): bool
    {
        if (isset($this->targets[$id])) {
            return true;
        }

        return false;
    }
}
