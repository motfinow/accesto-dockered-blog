<?php

namespace AppBundle\Service\BlogPost;

use AppBundle\Entity\BlogPost;
use AppBundle\Event\BlogPostCreatedEvent;
use AppBundle\Event\BlogPostUpdatedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlogPostManager
{
    protected $entityManger;
    protected $eventDispatcher;

    public function __construct(
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->entityManger = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param BlogPost $blogPost
     */
    public function saveNew(BlogPost $blogPost): void
    {
        $this->entityManger->persist($blogPost);
        $this->entityManger->flush();

        $blogPostCreated = new BlogPostCreatedEvent($blogPost);

        $this->eventDispatcher->dispatch('blog_post.created', $blogPostCreated);
    }

    /**
     * @param BlogPost $blogPost
     */
    public function update(BlogPost $blogPost): void
    {
        $this->entityManger->flush();

        $blogPostUpdated = new BlogPostUpdatedEvent($blogPost);

        $this->eventDispatcher->dispatch('blog_post.updated', $blogPostUpdated);
    }
}
