<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\BlogPost;
use AppBundle\Exception\TargetNotExistsException;
use AppBundle\Form\Type\BlogPostType;
use AppBundle\Service\BlogPost\BlogPostManager;
use AppBundle\Service\BlogPost\TargetContainer;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BlogPostController.
 */
class BlogPostController extends FOSRestController
{
    /**
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Return complete list of blog posts"
     * )
     *
     * @Route(name="api.blog_post.list", path="/blog-post")
     * @Method("GET")
     *
     * @return \FOS\RestBundle\View\View
     */
    public function listPostsAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:BlogPost');

        return $this->view($repo->findAll());
    }

    /**
     * @ApiDoc(
     *     input="AppBundle\Form\Type\BlogPostType",
     *     output="AppBundle\Entity\BlogPost",
     *     section="Blog Post",
     *     description="Create blog post",
     *     statusCodes={
     *          201 = "Succesfully created",
     *          400 = "Invalid data"
     *     }
     * )
     *
     * @Route(name="api.blog_post.create", path="/blog-post")
     * @Method("POST")
     * @param Request $request
     * @param BlogPostManager $blogPostManager
     *
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\Form
     */
    public function createPostAction(Request $request, BlogPostManager $blogPostManager)
    {
        $form = $this->createForm(BlogPostType::class, null, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $blogPost = $form->getData();
        $blogPostManager->saveNew($blogPost);

        return $this->view($blogPost, Response::HTTP_CREATED);
    }


    /**
     * @ApiDoc(
     *     input="AppBundle\Form\Type\BlogPostType",
     *     output="AppBundle\Entity\BlogPost",
     *     section="Blog Post",
     *     description="Update blog post",
     *     statusCodes={
     *          204 = "Succesfully updated",
     *          400 = "Invalid data",
     *          404 = "Blog post not found"
     *     }
     * )
     *
     * @Route(name="api.blog_post.update", path="/blog-post/{post}")
     * @Method("PUT")
     * @param Request $request
     * @param BlogPost $post
     * @param BlogPostManager $blogPostManager
     *
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\Form
     */
    public function updatePostAction(
        Request $request,
        BlogPost $post,
        BlogPostManager $blogPostManager
    ) {
        $form = $this->createForm(BlogPostType::class, $post, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $blogPost = $form->getData();
        $blogPostManager->update($blogPost);

        return $this->view($blogPost, Response::HTTP_NO_CONTENT);
    }

    /**
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Publish post to specified target",
     *     statusCodes={
     *          204 = "Succesfully published",
     *          400 = "Invalid target service",
     *          404 = "Blog post not found",
     *     }
     * )
     * @Route(name="api.blog_post.publish", path="/blog-post/{post}/{target}")
     * @Method("POST")
     * @param BlogPost $post
     * @param $target
     *
     * @return \FOS\RestBundle\View\View
     * @throws TargetNotExistsException
     */
    public function publishPostAction(BlogPost $post, $target, TargetContainer $targetContainer)
    {
        $targetService = $targetContainer->get($target);

        $targetService->publish($post);

        return $this->view($post, Response::HTTP_NO_CONTENT);
    }
}
