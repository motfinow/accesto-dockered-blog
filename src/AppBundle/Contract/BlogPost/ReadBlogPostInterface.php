<?php

namespace AppBundle\Contract\BlogPost;

/**
 * Interface ReadBlogPostInterface
 * @package AppBundle\Contract\BlogPost
 */
interface ReadBlogPostInterface
{
    public function getId();

    public function getTitle();

    public function getContent();

    public function getTags();
}
