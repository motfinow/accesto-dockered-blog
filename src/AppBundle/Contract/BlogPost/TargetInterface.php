<?php

namespace AppBundle\Contract\BlogPost;

/**
 * Interface TargetInterface
 * @package AppBundle\Contract\BlogPost
 */
interface TargetInterface
{
    public function getId(): string;

    public function publish(ReadBlogPostInterface $blogPost): void;
}
