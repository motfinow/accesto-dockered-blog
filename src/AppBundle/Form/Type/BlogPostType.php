<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\BlogPost;
use AppBundle\Form\DataTransformer\StringWithCommasToArrayTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogPostType extends AbstractType
{
    /**
     * @var StringWithCommasToArrayTransformer
     */
    protected $tagsToArrayTransformer;

    public function __construct(StringWithCommasToArrayTransformer $stringWithCommasToArrayTransformer)
    {
        $this->tagsToArrayTransformer = $stringWithCommasToArrayTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content')
            ->add('tags');

        $builder->get('tags')
            ->addModelTransformer($this->tagsToArrayTransformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogPost::class,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blog_post';
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
