<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class StringWithCommasToArrayTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $value
     * @return mixed|string
     */
    public function transform($value)
    {
        if (is_array($value)) {
            return implode(',', $value);
        }

        return $value;
    }

    /**
     * @param mixed $value
     * @return array|mixed
     */
    public function reverseTransform($value)
    {
        return explode(',', $value);
    }
}
